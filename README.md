# my-project

Pour que le site marche, il faut installer:
    npm install netlify-cli -g

Pour lancer le site:
    netlify dev


## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
