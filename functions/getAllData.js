const sendQuery = require('./utils/sendQuery')
const faunadb = require('faunadb')
const queryAllData = require('./utils/queryAllData')
const q = faunadb.query
const client = new faunadb.Client({
    secret: process.env.FAUNADB_SECRET
})

module.exports.handler = async (event, context, callback) => {
    try {
        const data = await queryAllData(client, q);   
        callback(null, sendQuery(200, data));
    } catch (err) {
        console.error(err)
        callback(null, sendQuery(500, {error: err}))
    }
}
