const sendQuery = require('./utils/sendQuery')
const queryByCategory = require('./utils/queryByCategory')
const faunadb = require('faunadb')
const q = faunadb.query
const client = new faunadb.Client({
    secret: process.env.FAUNADB_SECRET
})

module.exports.handler = async (event, context, callback) => {
    try {
        const data = await queryByCategory(client, q, "all_body_works_name")
        callback(null, sendQuery(200, data));

    } catch (err) {
        console.error(err)
        callback(null, sendQuery(500, { error: err }))

    }
}
