//import store from '../src/store/modules/results'
const sendQuery = require('./utils/sendQuery')

const faunadb = require('faunadb')
const q = faunadb.query
const client = new faunadb.Client({
    secret: process.env.FAUNADB_SECRET
})

module.exports.handler = async (event, context, callback) => {
    try {
        const { data } = await client.query(
            q.Distinct(
                q.Map(
                    q.Paginate(
                        q.Intersection(
                            createFilterRequest(event),
                        )
                    ),
                    q.Lambda("results", q.Get(q.Var("results")))
                )
            ))
        callback(null, sendQuery(200, data));

    } catch (err) {
        console.error(err)
        callback(null, sendQuery(500, { error: err }))

    }
}



// This function will dynamicly create a request to the database
function createFilterRequest(event) {

    const brandsList = event.queryStringParameters.brand.split(',');
    const modelsList = event.queryStringParameters.model.split(',');
    const motorizationList = event.queryStringParameters.motorization.split(',');
    const bodyWorkList = event.queryStringParameters.bodyWork.split(',');
    const numberOfPlacesList = event.queryStringParameters.numberOfPlace.split(',');

    const co2Rate = parseInt(event.queryStringParameters.co2Rate);
    const minPrice = parseInt(event.queryStringParameters.minPrice);
    const maxPrice = parseInt(event.queryStringParameters.maxPrice);

    const requestBrand = [];
    const requestModel = [];
    const requestMotorization = [];
    const requestbodyWork = [];
    const requestNumberOfPlace = [];

    const expTags = [];

    const expsFinal = [];

    if (brandsList[0] !== "" || modelsList[0] !== "" || motorizationList[0] !== "" || bodyWorkList[0] !== "" || numberOfPlacesList[0] !== "") {
        if (brandsList[0] !== "") {
            brandsList.forEach(element => {
                requestBrand.push(q.Match(q.Index("all_results_by_brand"), element))
            });
            expTags.push(q.Union(requestBrand));
        }

        if (modelsList[0] !== "") {
            modelsList.forEach(element => {
                requestModel.push(q.Match(q.Index("all_results_by_model"), element))
            });
            expTags.push(q.Union(requestModel));
        }

        if (motorizationList[0] !== "") {
            motorizationList.forEach(element => {
                requestMotorization.push(q.Match(q.Index("all_results_by_motorization"), element))
            });
            expTags.push(q.Union(requestMotorization));
        }

        if (bodyWorkList[0] !== "") {
            bodyWorkList.forEach(element => {
                requestbodyWork.push(q.Match(q.Index("all_results_by_body_work"), element))
            });
            expTags.push(q.Union(requestbodyWork));
        }

        if (numberOfPlacesList[0] !== "") {
            numberOfPlacesList.forEach(element => {
                requestNumberOfPlace.push(q.Match(q.Index("all_results_by_number_of_places"), element))
            });
            expTags.push(q.Union(requestNumberOfPlace));
        }

        expsFinal.push(q.Join(q.Intersection(expTags),
            q.Lambda(["ref"], q.Match(q.Index("all_results_by_ref"), q.Var("ref")))))
    }

    if (!isNaN(co2Rate)) {

        expsFinal.push(q.Join(
            q.Range(q.Match(q.Index("all_results_by_max_co2")), 0, co2Rate),
            q.Lambda(
                ["weight", "ref"],
                q.Match(q.Index("all_results_by_ref"), q.Var("ref"))
            )
        ))

    }
    expsFinal.push(q.Join(
        q.Range(q.Match(q.Index("all_results_by_price")), minPrice, maxPrice),
        q.Lambda(
            ["price", "ref"],
            q.Match(q.Index("all_results_by_ref"), q.Var("ref"))
        )
    ))

    /*
    if (brandsList[0] === "" && modelsList[0] === "" && motorizationList[0] === ""  && isNaN(co2Rate) && bodyWorkList[0] === ""  && numberOfPlacesList[0] === ""  ) {
        expTags.push(q.Match(q.Index("all_results")))
        return expTags;
    }*/

    return expsFinal;

}