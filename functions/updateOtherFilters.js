const sendQuery = require('./utils/sendQuery')

const faunadb = require('faunadb')
const q = faunadb.query
const client = new faunadb.Client({
    secret: process.env.FAUNADB_SECRET
})

module.exports.handler = async (event, context, callback) => {
    try {  
        const { data } = await client.query(
            q.Distinct(
                q.Map(
                    q.Paginate(
                        q.Intersection(
                            createFilterRequest(event),
                        )
                    ),
                    q.Lambda("results", q.Get(q.Var("results")))
                )
            ))
        callback(null, sendQuery(200, data));

    } catch (err) {
        console.error(err)
        callback(null, sendQuery(500, { error: err }))

    }
}

function createFilterRequest(event) {
    const brandsList = event.queryStringParameters.brand.split(',');
    const requestBrand = [];
    const expsFinal = [];
    brandsList.forEach(element => {
        requestBrand.push(q.Match(q.Index("all_results_by_brand"), element))
    });
    expsFinal.push(q.Union(requestBrand));

    return expsFinal;
}