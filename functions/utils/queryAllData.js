module.exports = async (client ,q) => {
    const  {data} =  await client.query(
        q.Map(
            q.Paginate(
                q.Match(q.Index("all_results"))
            ),
            q.Lambda("getAllData", q.Get(q.Var("getAllData")))
        )
    )
    return data;
}