module.exports = async (client ,q, FaunaIndex) => {
    const { data } = await client.query(
        q.Max(q.Paginate(q.Distinct(q.Match(q.Index(FaunaIndex)))))
    )
    return data;
}
