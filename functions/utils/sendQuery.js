module.exports = (statusCode, data) => {
    return {
        statusCode: statusCode,
        headers: {
            /* Required for CORS support to work */
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Methods": "POST, OPTIONS",
        },
        body: JSON.stringify(data),
    }
}