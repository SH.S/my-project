import Vuex from 'vuex';
import Vue from 'vue';
import results from './modules/results'

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        results
    }
})