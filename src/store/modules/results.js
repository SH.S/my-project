const state = {
    // all the results in the database
    tableResults: [],

    // all brands, models, motorizations in the database
    allBrands: [],
    allModels: [],
    allMotorizations: [],
    allBodyWorks: [],
    allNumberOfPlaces: [],
    
    // available models, motorizations, bodyWork, number of places for each brand selected
    availableModels: [],
    availableMotorizations: [],
    availableBodyWorks: [],
    availableNumberOfPlaces: [],

    // all brands, models, motorizations that chosen to be used for filters
    brandFilters: [],
    modelFitlers: [],
    motorizationFilters: [],
    bodyWorkFilters: [],
    numberOfPlacesFilters: [],

    perPage: 5,
    co2Rate: undefined,

    minPrice: undefined,
    maxPrice: undefined,

    loading: false,
    loadingFilters: false,

    maxPriceAvailable: undefined,
    maxCo2Available: undefined,

    colorButton: "outline-success"

};

const getters = {
    allResults: (state) => state.tableResults,

    allBrands: (state) => state.allBrands,
    allModels: (state) => state.allModels,
    allMotorizations: (state) => state.allMotorizations,
    allBodyWorks: (state) => state.allBodyWorks,
    allNumberOfPlaces:(state) => state.allNumberOfPlaces,

    availableModels: (state) => state.availableModels,
    availableMotorizations: (state) => state.availableMotorizations,
    availableBodyWorks: (state) => state.availableBodyWorks,
    availableNumberOfPlaces: (state) => state.availableNumberOfPlaces,

    filtersBrand: (state) => state.brandFilters,

    loadingState: (state) => state.loading,
    loadingFiltersState: (state) => state.loadingFilters,

    perPage: (state) => state.perPage,
    co2Rate: (state) => state.co2Rate,

    minPrice: (state) => state.minPrice,
    maxPrice: (state) => state.maxPrice,

    maxPriceAvailable: (state) => state.maxPriceAvailable,
    maxCo2Available: (state) => state.maxCo2Available,

    colorButton: (state) => state.colorButton
};

const actions = {
    async fetchResults({ commit }) {
        try {
            state.loading = true;
            const response = await fetch("/.netlify/functions/getAllData");
            const results = await response.json();
            const dataResults = results.map(element => element.data)

            commit('setResults', dataResults);
            state.loading = false;

        } catch (e) {
            console.error(e);
        }
    },

    async fetchBrandResults({ commit }) {
        try {
            const response = await fetch("/.netlify/functions/getBrand");
            const results = await response.json();
            commit('setBrands', results);

        } catch (e) {
            console.error(e);
        }
    },

    async fetchModelResults({ commit }) {
        try {
            const response = await fetch("/.netlify/functions/getModels");
            const results = await response.json();
            commit('setModels', results);
            commit('setAvailableModels', results);

        } catch (e) {
            console.error(e);
        }
    },

    async fetchMotorizationResults({ commit }) {
        try {
            const response = await fetch("/.netlify/functions/getMotorizations");
            const results = await response.json();
            commit('setMotorizations', results);
            commit('setAvailableMotorizations', results);

        } catch (e) {
            console.error(e);
        }
    },

    async fetchBodyWorkResults({ commit }) {
        try {
            const response = await fetch("/.netlify/functions/getBodyWorks");
            const results = await response.json();

            commit('setBodyWorks', results);
            commit('setAvailableBodyWorks', results);


        } catch (e) {
            console.error(e);
        }
    },

    async fetchNumberOfPlacesResults({ commit }) {
        try {
            const response = await fetch("/.netlify/functions/getNumberOfPlaces");
            const results = await response.json();

            commit('setNumberOfPlaces', results);
            commit('setAvailableNumberOfPlaces', results);


        } catch (e) {
            console.error(e);
        }
    },

    async updateTable({ commit }) {
        try {
            state.loading = true;
            const response = await fetch("/.netlify/functions/updateData" + "?brand=" + state.brandFilters + "&model=" + state.modelFitlers + "&motorization=" + state.motorizationFilters  + "&bodyWork=" + state.bodyWorkFilters + "&numberOfPlace=" + state.numberOfPlacesFilters + "&co2Rate=" + state.co2Rate + "&minPrice=" + state.minPrice + "&maxPrice=" + state.maxPrice);
            const results = await response.json();
            const dataResults = results.map(element => element.data);

            commit('setResults', dataResults);
            state.loading = false;

        } catch (e) {
            console.error(e);
        }
    },


    async updateOtherFilters({ commit }) {
        try {
            state.loadingFilters = true;
            const response = await fetch("/.netlify/functions/updateOtherFilters" + "?brand=" + state.brandFilters + "&model=" + [] + "&motorization=" + [] + "&bodyWork=" + [] + "&numberOfPlace=" + [] );
            const results = await response.json();

            const models = [];
            const motorizations = [];
            const bodyWorks = [];
            const numberOfPlaces = [];

            results.forEach(element => {
                if (!models.includes(element.data.Modeles)) {               // A VOIR POUR TROUVER UNE SOLUTION PLUS EFFICACE
                    models.push(element.data.Modeles);
                }                                                           // UNE SOLUTION CA SERAI DE FAIRE DES INDEX COMME ALL_RESULTS_BY_... MAIS AVEC SEULEMENT LES DONNEES QU'ON VEUT
                if (!motorizations.includes(element.data.Motorisation)) {
                    motorizations.push(element.data.Motorisation);
                }
                if (!bodyWorks.includes(element.data.Carrosserie)) {
                    bodyWorks.push(element.data.Carrosserie);
                }
                if (!numberOfPlaces.includes(element.data.Nb_Places)) {
                    numberOfPlaces.push(element.data.Nb_Places);
                }
            });

            commit('setAvailableModels', models);
            commit('setAvailableMotorizations', motorizations);
            commit('setAvailableBodyWorks', bodyWorks);
            commit('setAvailableNumberOfPlaces', numberOfPlaces);

            state.loadingFilters = false;

        } catch (e) {
            console.error(e);
        }
    },

    async fetchMaxPriceAvailable ({commit}) {
        const response = await fetch("/.netlify/functions/getMaxPriceAvailable");
        const results = await response.json();

        commit('setMaxPriceAvailable', results[0]);
    },

    async fetchMaxCo2Available ({commit} ) {
        const response = await fetch("/.netlify/functions/getMaxCo2Available");
        const results = await response.json();

        commit('setMaxCo2eAvailable', results[0]);
    },


    async updateLoadingState({ commit }, value) {
        commit('setLoadingState', value);
    },

    async updateLoadingFiltersState({ commit }, value) {
        commit('setLoadingFiltersState', value);
    },

    async updateBrandFilter({ commit }, brnad) {
        commit('setBrandFilter', brnad);
    },

    async updateModelFilter({ commit }, brnad) {
        commit('setModelFilter', brnad);
    },

    async updateMotorizationFilter({ commit }, value) {
        commit('setMotorizationFilter', value);
    },

    async updateBodyWorkFilter({ commit }, value) {
        commit('setBodyWorkFilter', value)
    },

    async updateNumberOfPlaceskFilter({ commit }, value) {
        commit('setNumberOfPlaceFilter', value)
    },

    async updatePerPage({ commit }, nb) {
        commit('setPerPage', nb);
    },

    async updateAvailableModels({ commit }, value) {
        commit('setAvailableModels', value);
    },

    async updateAvailableMotorizations({ commit }, value) {
        commit('setAvailableMotorizations', value);
    },

    async updateAvailableBodyWorks({ commit }, value) {
        commit('setAvailableBodyWorks', value);
    },

    async updateAvailableNumberOfPlaces({ commit }, value) {
        commit('setAvailableNumberOfPlaces', value);
    },

    async updateColorButton({ commit }, color) {
        commit('setColorButton', color);
    },

    async updateCo2Rate({ commit }, value) {
        commit('setCo2Rate', value);
    },

    async updateMinPrice({ commit }, value) {
        commit('setMinPrice', value);
    },

    async updateMaxPrice({ commit }, value) {
        commit('setMaxPrice', value);
    },

     


};

const mutations = {
    setResults: (state, results) => (state.tableResults = results),

    setBrands: (state, results) => (state.allBrands = results),
    setModels: (state, results) => (state.allModels = results),
    setMotorizations: (state, results) => (state.allMotorizations = results),
    setBodyWorks: (state, results) => (state.allBodyWorks = results),
    setNumberOfPlaces: (state, results) => (state.allNumberOfPlaces = results),


    setAvailableModels: (state, results) => (state.availableModels = results),
    setAvailableMotorizations: (state, results) => (state.availableMotorizations = results),
    setAvailableBodyWorks: (state, results) => (state.availableBodyWorks = results),
    setAvailableNumberOfPlaces: (state, results) => (state.availableNumberOfPlaces = results),

    setBrandFilter: (state, results) => (state.brandFilters = results),
    setModelFilter: (state, results) => (state.modelFitlers = results),
    setMotorizationFilter: (state, results) => (state.motorizationFilters = results),
    setBodyWorkFilter: (state, results) => (state.bodyWorkFilters = results),
    setNumberOfPlaceFilter: (state, results) => (state.numberOfPlacesFilters = results), 

    setLoadingState: (state, value) => (state.loading = value),
    setLoadingFiltersState: (state, value) => (state.loadingFilters = value),

    setPerPage: (state, value) => (state.perPage = value),

    setColorButton: (state, color) => (state.colorButton = color),

    setMinPrice: (state, value) => (state.minPrice = value),
    setMaxPrice: (state, value) => (state.maxPrice = value),


    setMaxPriceAvailable: (state, value) => (state.maxPriceAvailable = value),
    setMaxCo2eAvailable: (state, value) => (state.maxCo2Available = value),

    setCo2Rate: (state, value) => (state.co2Rate = value),
};

export default {
    state,
    getters,
    actions,
    mutations
};